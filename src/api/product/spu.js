import request from '@/utils/request'

//获取SPU列表接口的数据
//    /admin/product/{page}/{limit}

export const reqSpuList = (page, limit, category3Id) => request({
    url: `/admin/product/${page}/${limit}`,
    method: 'get',
    params: { category3Id }
})

//获取spu信息
//      /admin/product/getSpuById/{spuId}
export const reqSpu = (spuId) => request({
    url: `/admin/product/getSpuById/${spuId}`,
    method: 'get'
})

//获取品牌地址
//      /admin/product/baseTrademark/getTrademarkList
export const reqTrademarkList = () => request({
    url: `/admin/product/baseTrademark/getTrademarkList`,
    method: 'get'
})

//获取拼品牌图片地址
//      /admin/product/spuImageList/{spuId}
export const reqSpuImageList = (spuId) => request({
    url: `/admin/product/spuImageList/${spuId}`,
    method: 'get'
})

//获取平台全部销售属性---整个平台销售属性一共三个
//      /admin/product/baseSaleAttrList
export const reqBaseSaleAttrList = () => request({
    url: `/admin/product/baseSaleAttrList`,
    method: 'get'
})

//修改spu
export const reqAddOrUpdateSpu = (spuInfo) => {
    //判断是否携带id
    if (spuInfo.id) {
        return request({
            url: `/admin/product/updateSpuInfo`,
            method: 'post',
            data: spuInfo
        })
    } else {
        return request({
            url: `/admin/product/saveSpuInfo`,
            method: 'post',
            data: spuInfo
        })
    }
}

//删除spu
export const reqDeleteSpu = (spuId) => request({
    url: `/admin/product/deleteSpu/${spuId}`,
    method: 'delete'
})

//获取图片接口
export const reqSpuImsgeList = (spuId) => request({
    url: `/admin/product/spuImageList/${spuId}`,
    method: 'get'
})

// 获取销售属性
export const reqSpuSaleAttrList = (spuId) => request({
    url: `/admin/product/spuSaleAttrList/${spuId}`,
    method: 'get'
})

//获取平台属性
export const reqAttrInfoList = (category1Id, category2Id, category3Id) => request({
    url: `/admin/product/attrInfoList/${category1Id}/${category2Id}/${category3Id}`,
    method: 'get'
})

//保存填写的sku信息
export const reqSaveSkuInfo = (skuInfo) => request({
    url: `/admin/product/saveSkuInfo`,
    method: 'post',
    data: skuInfo
})

//展示SKU列表数据接口
export const reqSkuList = (spuId) => request({
    url: `/admin/product/findBySpuId/${spuId}`,
    method: 'get'
})