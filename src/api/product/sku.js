import request from '@/utils/request'

//获取SKU列表数据
export const reqSkuList = (page, limit) => request({
    url: `/admin/product/list/${page}/${limit}`,
    method: 'get'
})

//上架请求
export const reqOnSale = (skuId) => request({
    url: `/admin/product/onSale/${skuId}`,
    method: 'get'
})

//下架架请求
export const reqCancelSale = (skuId) => request({
    url: `/admin/product/cancelSale/${skuId}`,
    method: 'get'
})

//抽屉框数据请求
export const reqSkuDrawer = (skuId) => request({
    url: `/admin/product/getSkuById/${skuId}`,
    method: 'get'
})

//删除一列sku数据
export const reqDeleteSku = (skuId) => request({
    url: `/admin/product/deleteSku/${skuId}`,
    method: 'delete'
})