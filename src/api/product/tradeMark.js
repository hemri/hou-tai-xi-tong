import request from '@/utils/request'
//  /admin/product/baseTrademark/{page}/{limit}

export const reqTradeMarkList = (page, limit) => request({
    url: `/admin/product/baseTrademark/${page}/${limit}`,
    method: 'get'
})

//处理添加品牌      post   携带两个参数：品牌LOG：logoUrl  品牌名称：tmName
// 不需要传递id，id由服务器生成
//  /admin/product/baseTrademark/save

//处理修改品牌      put    携带三个个参数：id   品牌LOG：logoUrl  品牌名称：tmName        
//  /admin/product/baseTrademark/update

export const reqAddOrUpdateTradeMark = (tradeMark) => {
    //是否携带id
    if (tradeMark.id) {
        // 修改品牌
        return request({
            url: '/admin/product/baseTrademark/update',
            method: 'put',
            data: tradeMark
        })
    } else {
        // 新增品牌
        return request({
            url: '/admin/product/baseTrademark/save',
            method: 'post',
            data: tradeMark
        })
    }
}

//删除品牌  
//  /admin/product/baseTrademark/remove/{id}
export const reqDeleteTradeMark = (id) => request({
    url: `/admin/product/baseTrademark/remove/${id}`,
    method: 'delete'
})